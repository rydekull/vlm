lang en_US
keyboard us
timezone Etc/UTC --isUtc
rootpw $1$eqScB9Ng$fnxoUz.JUy6bA7c/m26jm. --iscrypted
reboot
text
bootloader --location=mbr --append="rhgb quiet crashkernel=auto"
zerombr
clearpart --all --initlabel
autopart
auth --passalgo=sha512 --useshadow
selinux --enforcing
firewall --enabled --ssh
skipx
firstboot --disable
%packages
%end
